
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Access(AccessType.PROPERTY)
public class HandyWorker extends Actor {

	// Attributes

	private String								make;

	private Collection<Application>				applications;
	private Collection<Tutorial>				tutorials;
	private Curriculum							curriculum;
	private Finder								finder;
	private Collection<HandyWorkerEndorsement>	handyWorkerEndorsements;


	// Getters & setters

	@NotBlank
	public String getMake() {
		return this.make;
	}

	public void setMake(final String make) {
		this.make = make;
	}

	// Relationships ----------------------------------------------------------

	@OneToMany
	public Collection<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(final Collection<Application> applications) {
		this.applications = applications;
	}

	@OneToMany
	public Collection<Tutorial> getTutorials() {
		return this.tutorials;
	}

	public void setTutorials(final Collection<Tutorial> tutorials) {
		this.tutorials = tutorials;
	}

	@OneToOne(optional = true)
	public Curriculum getCurriculum() {
		return this.curriculum;
	}

	public void setCurriculum(final Curriculum curriculum) {
		this.curriculum = curriculum;
	}

	@OneToOne(optional = false)
	public Finder getFinder() {
		return this.finder;
	}

	public void setFinder(final Finder finder) {
		this.finder = finder;
	}

	@OneToMany
	public Collection<HandyWorkerEndorsement> getHandyWorkerEndorsements() {
		return this.handyWorkerEndorsements;
	}

	public void setHandyWorkerEndorsements(final Collection<HandyWorkerEndorsement> handyWorkerEndorsements) {
		this.handyWorkerEndorsements = handyWorkerEndorsements;
	}

}
