package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

@Embeddable
@Access(AccessType.PROPERTY)
public class Priority {
	
	// Attributes
	
	private String 		value;
	
	// Getters & setters
	
	@Pattern(regexp = "^(HIGH|NEUTRAL|LOW)$")
	public String getValue(){
		return value;
	}
	
	public void setValue(String value){
		this.value = value;
	}
}
