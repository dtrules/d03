*************************************************************************
********************************VERSION 0********************************
*************************************************************************
# Versi�n inicial entregada en la D02

*************************************************************************
********************************VERSION 1********************************
*************************************************************************
# Cambios de la v0 a la v1

Cambios en navegavilidad ---------------------------------

Cortada navegabilidad entre Curriculum y sus records.
Cortada navegabilidad entre HandyWorkerEndorsement - Customer y CustomerEndorsement - HandyWorker.
Cortada navegabilidad de complaint a Report.

Cambios en multiplicidad ---------------------------------

Relacion handyworker y curriculum, curriculum a 0..1 cambiada.
Relacion mensaje - actor, en actor a 1 cambiada.
Multiplicidad Task - Complaint a 1 - 0..* cambiada.
Relacion Folder - Message, folder a 1 cambiada.

Cambios en direcci�n -------------------------------------

Direcci�n entre Task y complaint cambiada.

Otros cambios --------------------------------------------

En curriculum, agregaci�n por composici�n cambiada.

*************************************************************************
********************************VERSION 2********************************
*************************************************************************
# Cambios de la v1 a la v2

Cambios en navegavilidad ---------------------------------

Cortada navegabilidad de Actor a Message: el actor se guarda en el mensaje, 
por lo que la X debe estar en el mensaje.
A�adida navegabilidad de Task a Application.

Cambios en multiplicidad ---------------------------------

Relaci�n Finder -> Task es 0..* a 0..* cambiada.
Relaci�n Sponsorship -> Tutorial es 0..* a 1 cambiada.
Relaci�n Actor -> Folder es 1 -> 0..* cambiada.

Cambios en direcci�n -------------------------------------

A�adida la punta de flecha de Actor a Folder.

Otros cambios --------------------------------------------

Ordenado el ReadMe v1 de forma que de verdad se pueda leer .
Cambiado el atributo "number" de CreditCard pasa de int a long.
Cambiado pattern de ticker a alphanumerico.
A�adido en Authority a HandyWorker, Sponsor y Referee.